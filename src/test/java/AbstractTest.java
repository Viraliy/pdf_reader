import lombok.extern.slf4j.*;
import org.junit.*;
import org.junit.rules.*;

import java.io.*;

@Slf4j
public class AbstractTest {
    @Rule public TestName name = new TestName();

    protected static final String TEXT_FILE = "TET-datasheet.pdf";
    protected static final String IMAGE_FILE = "superman.pdf";
    protected static final String TMP_DIR = "tmp" + File.separator;
    private long startTime;


    @Before
    public void before() {
        log.info("Start {} test. ", name.getMethodName());
        File file = new File(TMP_DIR);
        if(file.exists()) {
            deleteDir(file);
        }
        file.mkdir();
        startTime = System.currentTimeMillis();
    }

    @After
    public void after() {
        log.info("Test: \"{}\" finished for: {}ms", name.getMethodName(), System.currentTimeMillis() - startTime);
    }

    void deleteDir(File file) {
        File[] files = file.listFiles();
        if (files != null) {
            for (File f : files) {
                deleteDir(f);
            }
        }
        file.delete();
    }

}
