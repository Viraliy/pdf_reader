import com.pdf_to_html.converter.*;
import lombok.extern.slf4j.*;
import org.junit.*;

import java.io.*;

@Slf4j
public class Pdf2Html5Idr extends AbstractTest {

    @Test
    public void pdfToHtmlTest() {
        try {
            File fileForConvert = new File("src/test/resources/TET-datasheet.pdf");
            File outputDir = new File(TMP_DIR);
            AbstractPdfToHtmlConverter pdfToHtmlConverter = new PdfToHtmlConverter(new ConverterPreProcessor(), new ConverterPostProcessor());
            pdfToHtmlConverter.convert(fileForConvert, outputDir);
        } catch (Throwable e) {
            Assert.assertTrue(false);
            log.error("Test finished with exception. ", e);
        }
    }

}
