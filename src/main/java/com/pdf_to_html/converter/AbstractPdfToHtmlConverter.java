package com.pdf_to_html.converter;

import java.io.*;

public abstract class AbstractPdfToHtmlConverter {

    protected AbstractConverterPreProcessor preProcessor;
    protected AbstractConverterPostProcessor postProcessor;

    public AbstractPdfToHtmlConverter(AbstractConverterPreProcessor preProcessor, AbstractConverterPostProcessor postProcessor) {
        this.preProcessor = preProcessor;
        this.postProcessor = postProcessor;
    }
    public void convert(File fileForConvert, File outputDirectory) {
        File rootDirectory = new File(getRootDirectory(fileForConvert, outputDirectory));
        this.preProcessor.preProcess();
        convertFile(fileForConvert, outputDirectory);
        this.postProcessor.postProcess(rootDirectory);
    }

    protected abstract void convertFile(File fileForConvert, File outputDirectory);

    private String getRootDirectory(File fileForConvert, File outputDirectory) {
        String s = outputDirectory.getAbsoluteFile() + File.separator + fileForConvert.getName();
        int indexOfExtension = s.indexOf(".pdf");
        if(indexOfExtension == -1) {
            return s;
        } else {
            return s.substring(0, s.length() - 4);
        }
    }

}
