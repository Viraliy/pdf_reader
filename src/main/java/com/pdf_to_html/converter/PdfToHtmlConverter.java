package com.pdf_to_html.converter;

import org.jpedal.examples.html.*;
import org.jpedal.exception.*;
import org.jpedal.render.output.*;
import org.jpedal.render.output.html.*;

import java.io.*;

public class PdfToHtmlConverter extends AbstractPdfToHtmlConverter {

    public PdfToHtmlConverter(final AbstractConverterPreProcessor preProcessor,
                              final AbstractConverterPostProcessor postProcessor) {
        super(preProcessor, postProcessor);
    }

    @Override
    protected void convertFile(final File fileForConvert, final File outputDirectory) {
        HTMLConversionOptions conversionOptions = new HTMLConversionOptions();
        conversionOptions.setDisableComments(true);
        IDRViewerOptions viewerOptions = new IDRViewerOptions();

        PDFtoHTML5Converter converter = new PDFtoHTML5Converter(fileForConvert, outputDirectory, conversionOptions, viewerOptions);
        try {
            converter.convert();
        } catch (PdfException e) {
            throw new RuntimeException("Can not convert pdf file: " + fileForConvert.getAbsolutePath(), e);
        }
    }
}
