package com.pdf_to_html.converter;

import java.io.*;

public interface AbstractConverterPostProcessor {
    void postProcess(File rootDirectory);
}
