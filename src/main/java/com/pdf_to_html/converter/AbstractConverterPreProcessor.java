package com.pdf_to_html.converter;

public interface AbstractConverterPreProcessor {
    void preProcess();
}
