package com.pdf_to_html.converter;

import com.pdf_to_html.data.*;

import java.io.*;
import java.util.*;

public interface AbstractImageExtractor {
    Map<Integer, List<ImageData>> getImages(File rootDirectory);
}
