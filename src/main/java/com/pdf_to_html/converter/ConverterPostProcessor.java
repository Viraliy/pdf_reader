package com.pdf_to_html.converter;

import com.pdf_to_html.data.*;
import com.pdf_to_html.utils.*;

import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.*;

public class ConverterPostProcessor implements AbstractConverterPostProcessor {

    @Override
    public void postProcess(File rootDirectory) {
        ImageExtractor imageExtractor = new ImageExtractor();
        // extract images from svg files
        Map<Integer, List<ImageData>> imagesByPage = imageExtractor.getImages(rootDirectory);
        // remove image tags from svg files.
        imagesByPage.keySet()
                .forEach(pageId -> XmlParserUtils.dropTagsFromFileAndOverride(
                        FilesManipulationService.getSvgFileByPageId(rootDirectory, pageId), "image"));

        // add image tags to html files.
        imagesByPage.forEach((page, images) -> {
            StringBuilder imagesAsHtml = new StringBuilder();
            AtomicInteger counter = new AtomicInteger();
            images.forEach(image -> imagesAsHtml.append(image.toImageHtmlTag(counter.getAndIncrement())).append(System.lineSeparator()));

            File htmlFile = FilesManipulationService.getHtmlFileByPageId(rootDirectory, page);
            FileEditorUtils.insertBeforeAndUpdateFile(htmlFile, "<div id=\"pg" + page + "Overlay\"", imagesAsHtml.toString());
        });
    }
}
