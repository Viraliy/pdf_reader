package com.pdf_to_html.utils;

import org.apache.commons.io.*;
import org.w3c.dom.*;
import org.xml.sax.*;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import java.io.*;

public class XmlParserUtils {

    public static Document getDocument(File file) {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = getDocumentBuilder(dbFactory);
        try {
            return dBuilder.parse(file);
        } catch (SAXException | IOException e) {
            throw new RuntimeException("Failed to parse SVG file: ", e);
        }
    }

    public static void dropTagsFromFileAndOverride(final File file, String tag) {
        Document document = getDocument(file);
        NodeList images = document.getElementsByTagName(tag);
        while (images.getLength() != 0) {
            images.item(0).getParentNode().removeChild(images.item(0));
        }
        try {
            IOUtils.write(documentToString(document).getBytes("utf-8"), new FileOutputStream(file));
        } catch (IOException e) {
            throw new RuntimeException("Can not override file: " + file.getAbsolutePath(), e);
        } catch (TransformerException e) {
            throw new RuntimeException("Can not transform file: " + file.getAbsolutePath(), e);
        }
    }

    private static DocumentBuilder getDocumentBuilder(DocumentBuilderFactory factory) {
        try {
            return factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new RuntimeException("Failed to initiate document builder.", e);
        }
    }

    public static String documentToString(Document doc) throws TransformerException {
        DOMSource domSource = new DOMSource(doc);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.transform(domSource, result);
        return writer.toString();
    }

}
