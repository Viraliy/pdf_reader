package com.pdf_to_html.utils;

import org.apache.commons.io.*;

import java.io.*;

public class FileEditorUtils {
    public static void insertBeforeAndUpdateFile(File file, String insertBefore, String contentForInsert) {
        try {
            String htmlFile = IOUtils.toString(new FileInputStream(file), "utf-8");
            int index = htmlFile.indexOf(insertBefore);
            if(index != -1) {
                String resultFile = htmlFile.substring(0, index) + contentForInsert + htmlFile.substring(index,
                                                                                                           htmlFile.length() - 1);
                IOUtils.write(resultFile.getBytes("utf-8"), new FileOutputStream(file));
            } else {
                throw new IllegalStateException("Failed to insert data to file: " + file.getAbsolutePath() +
                                                ". Because string: " + insertBefore + " was missed.");
            }
        } catch (IOException e) {
            throw new RuntimeException("Failed to parse html file: " + file.getAbsolutePath(), e);
        }

    }
}
