package com.pdf_to_html.utils;

import com.google.common.collect.*;
import lombok.*;

import java.io.*;
import java.util.*;

@Data
@AllArgsConstructor
public class FilesManipulationService {
    private static final Set<String> EXCLUDE_DIRS = Sets.newHashSet("assets", "fonts", "thumbnails");
    private static final String SVG_TYPE = ".svg";
    private static final String HTML_TYPE = ".html";


    public static List<File> getPagesDirs(File rootDirectory) {
        List<File> result = new ArrayList<>();

        if (rootDirectory.isDirectory()) {
            for (File file : rootDirectory.listFiles()) {
                if (file.isDirectory() && !EXCLUDE_DIRS.contains(file.getName())) {
                    result.add(file);
                }
            }
        } else {
            throw new IllegalArgumentException("RootDirectory is file. Path: " + rootDirectory.getAbsolutePath());
        }
        return result;
    }

    public static File getSvgFileByPageId(File rootDirectory, @NonNull Integer pageId) {
        String filePath = rootDirectory.getAbsoluteFile() + File.separator + pageId + File.separator + pageId + SVG_TYPE;
        File file = new File(
                filePath);
        if(!file.exists()) {
            throw new IllegalStateException(new FileNotFoundException(filePath));
        }
        return file;
    }

    public static File getHtmlFileByPageId(File rootDirectory, @NonNull Integer pageId) {
        String filePath = rootDirectory.getAbsoluteFile() + File.separator + pageId + HTML_TYPE;
        File file = new File(
                filePath);
        if(!file.exists()) {
            throw new IllegalStateException(new FileNotFoundException(filePath));
        }
        return file;
    }

}
