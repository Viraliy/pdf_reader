package com.pdf_to_html.utils;

import com.pdf_to_html.converter.*;
import com.pdf_to_html.data.*;
import org.w3c.dom.*;

import java.io.*;
import java.util.*;

public class ImageExtractor implements AbstractImageExtractor {
    @Override
    public Map<Integer, List<ImageData>> getImages(final File rootDirectory) {
        Map<Integer, List<ImageData>> result = new HashMap<>();
        List<File> pages = FilesManipulationService.getPagesDirs(rootDirectory);

        pages.forEach(pageDirectory -> {
            Integer pageNumber = Integer.parseInt(pageDirectory.getName());
            result.put(pageNumber, new ArrayList<>());

            Document doc = XmlParserUtils.getDocument(FilesManipulationService.getSvgFileByPageId(rootDirectory, pageNumber));
            NodeList images = doc.getElementsByTagName("image");
            for (int i = 0; i < images.getLength(); i++) {
                result.get(pageNumber).add(ImageData.buildImageDataFromXmlNode(images.item(i), pageNumber));
            }
        });

        return result;
    }
}
