package com.pdf_to_html.data;

import lombok.*;
import org.w3c.dom.*;

@Data
@AllArgsConstructor
public class ImageData {
    private int x;
    private int y;
    private int w;
    private int h;
    private String path;

    public static ImageData buildImageDataFromXmlNode(Node node, Integer pageNumber) {
        NamedNodeMap attributes = node.getAttributes();
        return new ImageData(
                Integer.parseInt(attributes.getNamedItem("x").getNodeValue()),
                Integer.parseInt(attributes.getNamedItem("y").getNodeValue()),
                Integer.parseInt(attributes.getNamedItem("width").getNodeValue()),
                Integer.parseInt(attributes.getNamedItem("height").getNodeValue()),
                pageNumber + "/" + attributes.getNamedItem("xlink:href").getNodeValue()
        );

    }

    public String getCssStyle() {
        return "position: absolute;"
               + "width: " + this.w + "px;"
               + "height: " + this.h + "px;"
               + "left: " + this.x + "px;"
               + "top: " + (this.y) + "px;"
               + "margin: 0;";
    }

    public String toImageHtmlTag(int id) {
        StringBuilder builder = new StringBuilder();
        return builder.append("<img ")
                .append("id=").append("custom_img_").append(id).append(" ")
                .append("style=").append("\"").append(getCssStyle()).append("\"")
                .append("src=").append("\"").append(this.path).append("\"").append("></img>").toString();
    }

}
